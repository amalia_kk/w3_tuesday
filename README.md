# Pao Factory TDD project

This project is to explore the coding of a bread factory (pão).

We will cover topics including:
- Git, bitbucket
- Markdown
- Agile/ scrum principles
- TDD (test driven development)
- Unit tests
- Functional programming


Other principles:
- Separation of concerns
- Dry code



## TDD and Unit Tests

A unit test is a single test that tests a part of a function. Several tests together will help to ensure the functionality of the program, reduce technical debt and keep it maintainable. Genreally, code lives, entropy adds complexity and shortcuts lead to technical debt that can kill projects.

Technical debt is the concept of taking shortcuts by skipping documentation or not making unit test leading to unmanageable code. Other factors such as time, people leaving and updates all add to technical debt.


**Test Driven Developtment** 

This is a way of developing code that is very agile. It's the lightest implementation of agile- it ensures working code. You make the tests first and then you code the least amount needed to pass the test. 



### User stories and tests

Good user stories can be used to make your unit tests. These are usually done by `Three Amigos` + `Devs` + `Testers` + `Business analyst`. In our case, it'll just be us. 

**User story1** As a bread maker, I want to provide `flour` and `water` and add to my `make_dough_option` to get out `dough`, else I want `not dough`, so that I can bake bread.

**User story 2** As a bread maker, I want to be able to take `dough` and use the `bake_option` to get `Pão`. Else I should get `not Pão`. So that I can make bread. 

**User Story 3** As a break maker, I want an option of `run_factory` that will take in `flour` and `water` and give me `Pão`, else give me `not Pão`. So I can make break with one simple action. 


## Logics

Logical and
Logical `and` evaluates a condition on both sides, both need to be True to result in True
<condition> and <condition>
True and True  >results> True
False and True  >results> False
True and False  >results> False
False and False  >results> False

Logical or
Logical `or` evaluates a condition on both sides, one needs to be True to result in True
<condition> or <condition>
True or True  >results> True
False or True  >results> True
True or False  >results> True
False or False  >results> False