# Define functions, not for running or calling

def make_dough_option(arg1, arg2):
    if "flour" == arg1 and "water" == arg2:
        return 'dough'
    elif "water" == arg1 and "flour" == arg2:
        return "dough"
    else:
        return "not dough"
        


def get_pao(arg1, arg2):
    if "dough" == arg1 and "bake_dough" == arg2:
        return "Pao"
    elif "bake_dough" == arg1 and "dough" == arg2:
        return "Pao"
    else:
        return "Not pao"


def run_factory(arg1, arg2):
    if "flour" == arg1 and "water" == arg2:
        return "Pao"
    elif "flour" == arg2 and "water" == arg1:
        return "Pao"
    else:
        return "Not pao"