# Import functions and run tests
from Factory_functions import *

# What is a test?
# It's an assertion, with a known outcomes of a function that comes back 
# with True or False


# **User story1** As a bread maker, I want to provide `flour` and 
# `water` and add to my `make_dough_option` to get out `dough`, 
# else I want `not dough`, so that I can bake bread.

# print("test number 1a - make_dough_option with 'flour' and 'water' should equal 'dough'")
# print(make_dough_option('flour', 'water') == 'dough')
# print("test number 1b - make_dough_option with 'cement' and 'water' should equal 'not dough'")
# print(make_dough_option('cement', 'water') == 'not dough')



# User story 2 + make tests + functions
# **User story 2** As a bread maker, I want to be 
# able to take `dough` and use the `bake_option` to get `Pão`. 
# Else I should get `not Pão`. So that I can make bread.

# print("test number 2a - get_pao with 'dough' and 'bake_dough' should equal 'Pao'")
# print(get_pao('dough', 'bake_dough') == 'Pao')
# print("test number 2b - make_dough_option with 'cement' and 'water' should equal 'not dough'")
# print(get_pao('dough', 'oven') == 'Not pao')

# User story 3 + make tests + functions
# **User Story 3** As a break maker, I want an option of `run_factory` that will take in 
# `flour` and `water` and give me `Pão`, else give me `not Pão`. So I can make break with one simple action. 

print("test number 3a - run_factory with 'flour' and 'water' should equal 'Pao")
print(run_factory('flour', 'water') == 'Pao')
print("test 3b - run_factory with 'cement' and 'water' should equal 'Not pao'")
print(run_factory('cement', 'water') == 'Not pao')
print("test number 3c - run_factory with 'flourfd' and 'gdfwater' should equal 'Not pao")
print(run_factory('flourfd', 'gdfwater') == 'Not pao')